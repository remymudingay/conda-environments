# conda-environments

Repository to store conda environment.yml files.

## Environments

- molecule

```bash
conda create -n molecule python=3.6
conda activate molecule
pip install -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple ansible==2.9.11 'molecule[docker,lint]==3.0.8' molecule-vagrant testinfra  pytest-ordering ncclient netaddr jmespath csentry-inventory pywinrm jxmlease
conda env export > molecule_env.yml
```

## Updating

When updating the environment:

- Update the README as well with the packages installed.
- Try creating the environment both on OSX and Linux:

  ```bash
  conda env create --force -f molecule_env.yml -n test
  ```

  Some packages might differ on both platforms. You might have to remove the exact version.
  For example, use `python==2.7.15` instead of `python==2.7.15=h9fef7bc_0`.

  Or even remove some:

  - bcrypt
  - libgcc-ng
  - libstdcxx-ng

- Make sure updating the env is idempotent (this is done by gitlab-ci):

  ```bash
  conda env update -f molecule_env.yml
  ```

  The `conda env export` command replaces the "." in pip packages names by "-" making the update not idempotent.
  Following packages should have a "." in their name:

  - aspy.yaml
  - backports.functools-lru-cache
  - backports.ssl-match-hostname
  - ruamel.ordereddict
  - ruamel.yaml
  - ruamel.yaml.clib

  Installation will work otherwise but update will not be idempotent (pip will re-install them everytime).

## Deploy

When pushing a tag, the molecule environments are automatically updated on the gitlab-runners.

## License

BSD 2-clause
